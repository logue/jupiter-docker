# Jupiter Docker

これは、Minecraft PE向けのサーバープログラムであるNukkitの派生版のJupiterのDocker Imageのリポジトリです。

<https://hub.docker.com/r/logue/jupiter-docker/>

[![Travis](https://img.shields.io/travis/JupiterDevelopmentTeam/Jupiter.svg?style=flat)](https://travis-ci.org/JupiterDevelopmentTeam/Jupiter)
![Version](https://gitlab.com/logue/jupiter-docker/raw/badges/version.png)
![Protocol](https://gitlab.com/logue/jupiter-docker/raw/badges/protocol.png)

## このイメージの使い方

```sh
docker run -it -p 19132:19132/udp logue/jupiter-docker
```

## ボリューム

このサーバーのワーキングディレクトリは /srv/jupiter にマウントされ、設定ファイルや生成されたワールドなどのファイルシステムがマウントされます。

```sh
 docker run -v /home/[username]/jupiter:/srv/jupiter -p 19132:19132/udp logue/jupiter-docker
```

## Dockerfile

DockerfileはGitLabにホストされています. [GitHubのJupiterのリポジトリ](https://github.com/JupiterDevelopmentTeam/Jupiter)はGitLabにミラーしています。 
[ミラーされたリポジトリ](https://gitlab.com/logue/jupiter)は、Jupiterリポジトリ更新時に自動的に同期され、Dockerイメージとともにリビルドされます。

## タグ

プロトコル番号を明示的に指定したい場合は、以下のようにコマンドを実行します。

```sh
docker pull logue/jupiter-docker:83
```

Minecraft PEのバージョンのように指定することもできます。

```sh
logue/jupiter-docker:0.15.6
```

## 問題点

Dockerイメージに関する問題などがありましたら[GitLab issue](https://gitlab.com/logue/jupiter-docker/issues)まで報告お願いします。

## Contributing

You are invited to contribute new features, fixes, or updates, large or small; we are always thrilled to receive pull requests, and do our best to process them as fast as we can.

Before you start to code, we recommend discussing your plans through a [GitLab issue](https://gitlab.com/logue/jupiter-docker/issues), especially for more ambitious contributions. This gives other contributors a chance to point you in the right direction, give you feedback on your design, and help you find out if someone else is working on the same thing.